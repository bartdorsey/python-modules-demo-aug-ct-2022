from shapes import rectangle, triangle

rect1_height = 10
rect1_width = 10

rect1_area = rectangle.area(rect1_height, rect1_width)

print(
    "The area of a rectangle with a height of",
    rect1_height,
    "and a width of",
    rect1_width,
    "is ",
    rect1_area,
)

rect2_height = 100
rect2_width = 200

rect2_area = rectangle.area(rect2_height, rect2_width)

print(
    "The area of a rectangle with a height of",
    rect2_height,
    "and a width of",
    rect2_width,
    "is ",
    rect2_area,
)

triangle1_height = 100
triangle1_width = 200

triangle1_area = triangle.area(triangle1_height, triangle1_width)

print(
    "The area of a triangle with a height of",
    triangle1_height,
    "and a width of",
    triangle1_width,
    "is ",
    triangle1_area,
)
